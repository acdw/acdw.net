# ATTENTION

**This is no longer the source code for my website.
I'm leaving it here for posterity and because I'm lazy.
To see the source of my website as of 2018-12-19, go to
<https://git.sr.ht/~acdw/acdw.net>.**


# acdw.net

*the personal website of Case Duckworth*

All the source files are in `src/`, and the publishable site is in `pub/`.  It
can be copied directly to a serve and hosted, no other configuration
necessary.

The only really interesting parts of this repo, code-wise, are the files in
`build/` (and maybe `assets/` if you're into chunky CSS).  Especially of
interest are `build/indexr`, which is a home-spun indexer capable of
generating yaml, html, markdown, and rss2-compatible xml from a list of files
(It's a work very much in progress, so use with caution, etc.) and the pandoc
filters in `filters/`.

Oh, and the `Makefile` I'm pretty proud of, as well.  Maybe I'll write a blog
post about it one of these days.

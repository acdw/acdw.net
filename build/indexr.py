#!/usr/bin/env python3

from datetime import date, timedelta
from textwrap import dedent
from itertools import chain
from re import search, DOTALL, sub
import argparse
import os.path
import sys


def log(*args):
    print(">", *args, file=sys.stderr)


FEED_TITLE = "acdw"
FEED_HREF = "https://www.acdw.net"
FEED_DESC = "Library tech by day, writer, skater, cook by night."

INDEX_NAME = "links"

BIRTHDAY = date(1990, 7, 25)
SITE_ROOT = 'https://www.acdw.net/'
FORMATS = {
    'html_list': {
        'fmt': '<li><a href="{href}">{title}</a></li>',
        # 'before': '', 'after': ''
    },
    'html_table': {
        'fmt': dedent("""\
                <tr>
                    <td><a href="{href}">{title}</a></td>
                    <td><a href="{group}/">{group}</a></td>
                    <td>{date}</td>
                </tr>
                """),
        'before': "<table>",
        'after': "</table>"
    },
    'md': {
        'fmt': '- [{href}]({title})',
        # 'before': '', 'after': ''
    },
    'yaml': {
        'fmt': dedent("""\
                - href: "{href}"
                  title: "{title}"
                  date: "{date}"
                  group: "{group}"\
                """),
        'before': '---\n{name}:',
        'after': '...'
    },
    'rss2': {
        'fmt': """
        <item>
            <title>{title}</title>
            <link>{href}</link>
            <description></description>
            <pubDate>{date}</pubDate>
            <content:encoded><![CDATA[{content}]]></content:encoded>
        </item>
        """,

        'before': """<?xml version="1.0" encoding="UTF-8"?>
        <rss version="2.0"
        xmlns:content="http://purl.org/rss/1.0/modules/content/">
        <channel>
            <title>{feed_title}</title>
            <link>{feed_href}</link>
            <description>{feed_desc}</description>
            <language>en-us</language>
            <lastBuildDate>{date}</lastBuildDate>
            """,

        'after': "</channel></rss>"
    }
}


class Post():
    '''A post object containing a title and a href, etc.'''

    def __init__(self, fname):
        '''Initialize the object by parsing the <title> element
        and using the filename as href.'''

        def get_title(doc):
            '''Get the <title> element from html.'''
            match = search(r'<title>(.*?)</title>', doc)
            return (match.group(1) if match else None)

        def get_group(doc):
            # XXX: this is really fragile.  It should be changed.
            # It has broken, so far: once (2018-03-11)
            match = search(r'<a class="p-category".*>(.*)</a>', doc)
            return (match.group(1) if match else None)

        def get_content(doc):
            '''Get the content of the article.'''
            match = search(r'<article.*>(.*)</article>', doc, flags=DOTALL)
            return (match.group(0) if match else None)

        with open(fname) as f:
            doc = f.read()
            self.title = get_title(doc) or fname
            self.content = get_content(doc) or ""
            self.group = get_group(doc) or ""

        self.date = os.path.splitext(os.path.basename(fname))
        self.href = SITE_ROOT + sub(r'^pub/', '', fname)

    def get_pubdate(self):
        '''Return the publication date of a post from its filename.'''
        try:
            d = int(self.date[0])
            return BIRTHDAY + timedelta(d)
        except ValueError:
            return 0

    def __lt__(self, other):
        '''Sort posts by date.'''
        return (self.date < other.date)

    def __eq__(self, other):
        '''Only posts with the same names should be equal to each other.'''
        return (self.fname == other.fname)


def write_list(post_list, fmt):
    '''Return a newline-delimited list using format specified.'''
    output = []

    if 'before' in fmt:
        output.append(fmt['before'].format(
            date=date.strftime(date.today(), "%F"),
            feed_title=FEED_TITLE,
            feed_href=FEED_HREF,
            feed_desc=FEED_DESC,
            name=INDEX_NAME
        ))

    for post in post_list:
        output.append(fmt['fmt'].format(
            href=post.href,
            title=post.title,
            date=post.get_pubdate(),
            content=post.content,
            group=post.group
        ))

    if 'after' in fmt:
        output.append(fmt['after'])

    return '\n'.join(output)


def parse_args(args):
    '''Return parsed argument namespace.'''
    parser = argparse.ArgumentParser()
    parser.add_argument("post", nargs="+", type=str,
                        help="Files to add to the index.")
    parser.add_argument("-f", "--format", type=str, default="yaml",
                        help="""Which format to use for link list.
                                Defaults to Pandoc's YAML metadata.
                                Possible values: {}
                                """.format(', '.join(f for f in FORMATS)))
    parser.add_argument("-r", "--remove", action="append", default=[],
                        help="""Words to remove from each title,
                                e.g. the site name.  Can be specified
                                more than once.""")
    parser.add_argument("-R", "--reverse", action="store_true",
                        help="Reverse the links' order in the index.")
    parser.add_argument("-x", "--exclude", action="append",
                        help="""Files to exclude from the index.
                                E.g., index.html.  Can be specified
                                more than once.""")
    parser.add_argument("--ftitle", type=str, default=FEED_TITLE,
                        help="""The title of the feed.""")
    parser.add_argument("--fhref", type=str, default=FEED_HREF,
                        help="""The link anchor of the feed.""")
    parser.add_argument("--fdesc", type=str, default=FEED_DESC,
                        help="""The description of the feed.""")
    parser.add_argument("-n", "--name", type=str, default=INDEX_NAME,
                        help="""The name of the generated list,
                                for use in filters.""")
    return parser.parse_args(args)


if __name__ == '__main__':
    args = parse_args(sys.argv[1:])

    remove = set(chain.from_iterable(r.split() for r in args.remove))

    FEED_TITLE = args.ftitle
    FEED_HREF = args.fhref
    FEED_DESC = args.fdesc

    INDEX_NAME = args.name

    idx = []
    for post in args.post:
        exclude = False
        if args.exclude is not None:
            for x in args.exclude:
                if search(x, post):
                    exclude = True
            if exclude:
                continue

        try:
            p = Post(post)
            p.title = ' '.join(w for w in p.title.split()
                               if w not in remove)
            idx.append(p)
        except FileNotFoundError:
            log("File not found:", post)
            continue
        except IsADirectoryError:
            log("Is a directory:", post)
            continue

    idx.sort(reverse=bool(args.reverse))

    if args.format not in FORMATS:
        log("Unknown format:", args.format, ".  Using default.")
        args.format = "yaml"

    print(write_list(idx, FORMATS[args.format]))

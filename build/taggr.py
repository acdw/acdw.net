#!/usr/bin/env python3

import yaml
import pprint
import sys
import os
sys.path.append(os.path.dirname(__file__))
from indexr import Post, write_list, FORMATS

DEBUG = True
BEFORE = """\
<!DOCTYPE html>
<!-- acdw.net -->
<html xmlns="https://www.w3.org/1999/xhtml"
      lang="en" dir="ltr">
<head>
    <title>{tag_name} | acdw</title>
    <meta charset="utf-8" />
    <meta name="generator" content="taggrv.01" />
    <meta name="viewport"
          content="width=device-width,
          initial-scale=1.0,
          user-scalable=yes" />
    <meta name="author" content="Case Duckworth" />
    <meta name="keywords"
          content="{tag_name}" />
    <link rel="stylesheet"
        media="screen"
        href="https://fontlibrary.org/face/cmu-typewriter"
        type="text/css" />
    <link rel="stylesheet"
        media="screen"
        href="https://fontlibrary.org/face/cmu-concrete"
        type="text/css" />
    <link rel="stylesheet"
        media="screen"
        href="/style/base.css"
        type="text/css" />
    <link rel="stylesheet"
        media="screen"
        href="/style/tag.css"
        type="text/css" />
    <link rel="alternate"
          type="application/rss+xml"
          href="/feed/all.rss"
          title="RSS feed for acdw.net" />
    <link rel="icon" type="image/png" href="/favicon.png" />
    <link rel="canonical" href="https://www.acdw.net/tag/{tag_name}" />
    <link rel="me" href="https://twitter.com/caseofducks" />
    <link rel="me" href="https://github.com/duckwork" />
    <link rel="me" href="mailto:me@acdw.net" />
    <link rel="openid.delegate" href="https://www.acdw.net" />
    <link rel="openid.server"
          href="https://openid.indieauth.com/openid" />
    <!--[if lt IE 9]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv-printshiv.min.js">
    </script>
    <![endif]-->
</head>
<body id="{tag_name}">
    <header class="site">
        <h1 class="site-title"><a href="/">acdw</a></h1>
        <h2 class="group-title">
            <a href="/tag/">tag</a>
        </h2>
        """
BEFORE_IDX = BEFORE + """\
        <div class="spacer"> </div>
    </header>
    <main class="site">
    <article>
    <ul class="tag-index">
"""

BEFORE_TAGS = BEFORE + """\
        <h3 class="page-title p-name">
            <a class="u-url" href="https://www.acdw.net/tag/{tag_name}">
            {tag_name}</a>
        <div class="spacer"> </div>
    </header>

    <main class="site">
    <article>
    <ul class="tag-index">
"""
AFTER = """\
    </ul>
    </article>
    </main>

    <footer class="site">
        <span class="bumper">
        Like this site, or just me as a person?  Feel free to
        <a class="underline" href="https://www.buymeacoffee.com/acdw">
            buy me a tea or something</a>!
        </span>
        <span class="copyright">&copy; 2018
        <a class="p-author h-card" href="https://www.acdw.net">
            Case Duckworth
        </a></span>
        <a rel="license" class="license"
        href="http://creativecommons.org/licenses/by-nc-sa/4.0/">
            <img
            alt="Creative Commons License" style="border-width:0"
            src="https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png"
            />
        </a>
    </footer>
</body>
</html>
"""


def debug(*args):
    if DEBUG:
        pp = pprint.PrettyPrinter(indent=4)
        pp.pprint(*args)


TAG_FILE = "build/include/tags.yaml"
TAG_DIR = "pub/tag"

if __name__ == '__main__':
    if sys.argv[1] is not None:
        TAG_DIR = sys.argv[1]

    with open(TAG_FILE) as f:
        tags = yaml.safe_load(f)["tags"]

    remove = {'|', 'acdw'}

    index_of_tags = os.path.join(TAG_DIR, 'index.html')
    idx_of_tags = []

    for tag in tags:
        tag_index = os.path.join(TAG_DIR, tag, 'index.html')
        tag_idx = []
        try:
            os.makedirs(os.path.dirname(tag_index))
        except FileExistsError:
            pass
        for post in tags[tag]:
            try:
                p = Post(post)
                p.title = ' '.join(w for w in p.title.split()
                                   if w not in remove)
                tag_idx.append(p)
            except FileNotFoundError:
                continue
            except IsADirectoryError:
                continue

            tag_idx.sort(reverse=True)
            with open(tag_index, 'w') as f:
                print(BEFORE_TAGS.format(tag_name=tag), file=f)
                print(write_list(tag_idx, FORMATS["html_list"]), file=f)
                print(AFTER, file=f)

        t = Post(tag_index)
        t.title = ' '.join(w for w in t.title.split()
                           if w not in remove)
        idx_of_tags.append(t)

    with open(index_of_tags, 'w') as f:
        print(BEFORE_IDX.format(tag_name='tag-index'), file=f)
        print(write_list(idx_of_tags, FORMATS["html_list"]), file=f)
        print(AFTER, file=f)

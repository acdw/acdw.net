--- Filters for posts.
-- These filters will apply to all post-type pages 
-- (as opposed to root pages or indexes).

-- What filters to include for this group?
local filters = {
    "auto-group-title", -- derive the group title from input file name
    "autolink", -- links posts to others written on the same day
    "blockquote-to-epigraph", -- converts a blockquote to a div.epigraph
    "ignorespan", -- ignores the string '..' (converts to span.ignore)
    "lineblock-verse", -- wraps lineblock lines in span.line s
    "slugify", -- generates a slug for the article (is this necessary?)
    "days-around", -- links to yesterday's and tomorrow's posts
    "format-datetime", -- formats the datetime provided to look nice
    "author-to-tags", -- converts the auto author field to tags
    "tagger", -- builds a tag file from the file's tags
}

local ret = {}
for _,f in ipairs(filters) do
    for _,elfunc in ipairs(require(f)) do
        table.insert(ret, elfunc)
    end
end

return ret

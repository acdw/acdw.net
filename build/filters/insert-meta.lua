-- Insert the metadata index into a document at {{index}}.
-- {{index}} should be in a paragraph by itself (needs to be a block
-- element).
-- vim: fdm=marker

local vars = {}

local function get_vars (meta)
    for k, v in pairs(meta) do
        vars["{{"..k.."}}"] = v
    end
    return nil
end

local function strCell(str)
    return { pandoc.Plain({pandoc.Str(str)}) }
end
function linkCell(content, target)
    return { pandoc.Plain({pandoc.Link(content, target)}) }
end
----------------------------------------------------------------------
local function insert_index(t) -- {{{ 
    local caption = {}
    local aligns = {
        pandoc.AlignDefault,
        pandoc.AlignDefault, 
        pandoc.AlignDefault,
    }
    local widths = { 0.0, 0.0, 0.0 }
    local headers = {{}, {}, {}}
    local rows = {}
    -- elements are group, date, title, href
    for _,item in ipairs(t) do
        local i = {}
        for e, c in pairs(item) do
            i[e] = pandoc.utils.stringify(item[e])
        end
        -- build the row
        rows[#rows + 1] = {
            linkCell(i.title, i.href),
            linkCell(i.group, i.group.."/"),
            strCell(i.date),
        }
    end
    local table = pandoc.Table(caption, aligns, widths, headers, rows)
    local attrs = pandoc.Attr("", {"index-links"}, {})
    return pandoc.Div(table, attrs)
end -- }}}

local function insert_readlist(t) -- {{{
    -- insert an unordered list of titles and authors,
    -- optionally with links
    -- spec: readlist: { 
    -- { title: "title",
    --   author: ["name", "name", ...],
    --   read: "<link to my blog post>",
    --   link: "<link to outside source>"
    -- }, ... }
    local list = {} -- here's the list we're building
    -- attributes for each element:
    local attrs = {
        title =  pandoc.Attr("",{"rl-title"}, {}),
        author = pandoc.Attr("",{"rl-author"},{}),
        link =   pandoc.Attr("",{"rl-link"},  {}),
    }
    for _,item in ipairs(t) do
        -- build a local formatted copy of info in the readlist
        local i = {} -- local copy
        local l = {} -- built list item
        for el, cont in pairs(item) do
            if el == "title" then
                i.title = pandoc.utils.stringify(cont)
            elseif el == "author" then
                local authors = {}
                local multiple = false
                for _,name in ipairs(item[el]) do
                    if type(name[1][1]) == "table" then 
                        multiple = true
                        for _,n in ipairs(name) do
                            table.insert(authors,pandoc.utils.stringify(n[1]))
                        end
                        table.insert(authors, "&")
                    else
                        table.insert(authors, pandoc.utils.stringify(name))
                    end
                end
                if multiple then authors[#authors] = nil end
                i.authors = table.concat(authors, " ")
            elseif el == "link" then
                i.link = pandoc.utils.stringify(cont)
            elseif el == "read" then
                i.read = pandoc.utils.stringify(cont)
            end
        end
        -- make local copies
        if i.read then
            l.title = pandoc.Link(i.title, i.read, "Read my review", attrs.title)
        else
            l.title = pandoc.Span(i.title, attrs.title)
        end
        if i.link then
            l.link = pandoc.Link("link", i.link, "Available here", attrs.link)
        else
            l.link = pandoc.Span("") 
        end
        l.author = pandoc.Span(i.authors, attrs.author)
        -- now put everything together into a list item
        list[#list + 1] = { pandoc.Plain({
            l.title, l.link, l.author,
        }) }
    end
    local bulletlist = pandoc.BulletList(list)
    local bulletattr = pandoc.Attr("",{"readlist"},{})
    return pandoc.Div(bulletlist,bulletattr)
end -- }}}

local function insert_roots(t)
    -- insert the pages we have on the blog
    local list = {}
    for _,item in ipairs(t) do
        local i = {}
        for e, c in pairs(item) do
            i[e] = pandoc.utils.stringify(item[e])
        end
        list[#list + 1] = { pandoc.Plain({
            pandoc.Link(i.title or "poop", i.href or "loop"),
        }) }
    end
    return pandoc.BulletList(list)
end
----------------------------------------------------------------------
local function table_replace (el)
    local e = pandoc.utils.stringify(el)
    local t = vars[e] 
    if t then
        -- XXX: this is really gnarly. I need a DSL or something
        if e == "{{links}}" then
            return insert_index(t)
        elseif e == "{{readlist}}" then
            return insert_readlist(t)
        elseif e == "{{roots}}" then
            return insert_roots(t)
        end
    end
end

return {
    { Meta = get_vars },
    { Para = table_replace },
}

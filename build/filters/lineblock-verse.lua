-- Add span.line s in LineBlocks
-- for this to work, include the following css:
-- .line-block br { display: none; }

return {
    {
        LineBlock = function (lb)
            local r = pandoc.LineBlock({})
            for _,l in pairs(lb.content) do
                local attrs = pandoc.Attr("", {"line"}, {})
                table.insert(r.content, { pandoc.Span(l, attrs) })
            end
            return r
        end,
    }
}

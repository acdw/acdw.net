local filters = {
    '_post', -- all the filters for posts
    'blockquote-to-epigraph', -- convert a blockquote to an epigraph.
}

local ret = {}
for _,f in ipairs(filters) do
    for _,elfunc in ipairs(require(f)) do
        table.insert(ret, elfunc)
    end
end

return ret

-- Format datetime from YYYY-mm-ddTHH:MM:ss-06
-- to something nicer
-- (as of now, YYYY-mm-dd)

function parse_dt(dt)
    if dt == nil then return end
    -- lucky for me, the string is always YYYY-mm-ddTHH:MM:ss-zz
    local match =    "(%d%d%d%d)%-"   -- year
    match = match .. "(%d%d)%-"     -- month
    match = match .. "(%d%d)"      -- day
    match = match .. "T(%d%d):"    -- hour
    match = match .. "(%d%d):"     -- minute
    match = match .. "(%d%d)"      -- second
    match = match .. "([+-]%d%d%d%d)" -- timezone
    return string.match(dt, match)
end

function format_dt(dt)
    -- eventually, it'd be nice to have this implement a full formatting DSL.
    -- but I don't have that skill right now, so I'll hard-code it.
    year, month, day = parse_dt(dt)
    return year .. "-" .. month .. "-" .. day
end

return {
    {
        Meta = function (m)
            if m.date then
                d = pandoc.utils.stringify(m.date)
            else return nil end
            if parse_dt(d) then
                m.datef = format_dt(d)
            end
            return m
        end,
    }
}

-- convert blockquotes to epigraphs

return {
    {
        BlockQuote = function (bq)
            local attrs = pandoc.Attr("", {"epigraph"}, {})
            return pandoc.Div(bq.content, attrs)
        end,
    }
}

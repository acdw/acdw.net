-- Ignore the string '..' by itself

return {
    {
        Str = function (e)
            if e.text == ".." then
                local attrs = pandoc.Attr("", {"ignore"}, {})
                return pandoc.Span("", attrs)
            else
                return e
            end
        end,
    }
}

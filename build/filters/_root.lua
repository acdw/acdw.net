-- Filters for roots.
-- These filters apply to root pages
-- (as opposed to posts or indexes).

-- What to include?
local filters = {
    "insert-meta", -- insert metadata tables at {{strings}}
}

local ret = {}
for _,f in ipairs(filters) do
    for _,elfunc in ipairs(require(f)) do
        table.insert(ret, elfunc)
    end
end

return ret

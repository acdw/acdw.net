-- Filters for indexes.
-- These filters apply to index pages
-- (as opposed to posts or roots).

-- What to include?
local filters = {
    "auto-group-title", -- derive the group title from input file name
    "insert-meta", -- insert metadata at {{templates}}
}

local ret = {}
for _,f in ipairs(filters) do
    for _,elfunc in ipairs(require(f)) do
        table.insert(ret, elfunc)
    end
end


return ret

-- Automatically link to other posts written on the same day

local lfs = require("lfs")

local function dir(str, part)
    -- return either (1) the dirpart or (2) the basename of str
    return string.gsub(str, "(.*/)(.*)", "%"..part)
end

local function ext(str, part)
    -- return either (1) the fname or (2) the extension of str
    return string.gsub(str, "(.*)%.(.*)", "%"..part)
end

local function daymatch(f1, f2)
    -- return True if fnames are the same but extensions
    -- are different
    samename = dir(ext(f1, 1), 2) == dir(ext(f2, 1), 2)
    diffextn = ext(f1, 2) ~= ext(f2, 2)
    return (samename and diffextn)
end

return {
    {
        Meta = function (m)
            -- requires to be fed <infile> with a -M switch
            m.lifeday = dir(ext(m.infile, 1), 2)
            m.othercats = {}
            for file in lfs.dir(lfs.currentdir().."/src/") do 
                if daymatch(m.infile, file) then
                    local l = ext(file, 2)
                    table.insert(m.othercats, l)
                end
            end
            if #m.othercats == 0 then m.othercats = nil end
            return m
        end,
    }
}

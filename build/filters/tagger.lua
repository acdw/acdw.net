-- tagger, with lua-yaml

local yaml = require("yaml")

local TAG_FILE = TAG_FILE or "tags.yaml"

local function is_empty(tbl)
    if type(tbl) ~= "table" then return false end
    if next(tbl) == nil then return true end
    for k, v in pairs(tbl) do
        if type(v) ~= "table" then return false end
        if type(k) ~= "number" then return false end
        if k == #tbl then 
            return is_empty(v)
        else
            if not is_empty(v) then return false end
        end
    end
end

local function convert_to_published(name)
    -- src/(.*).(.*) -> pub/\2/\1.html
    return name:gsub("src/(%d+)%.(.*)", "pub/%2/%1.html")
end

local function Set(list)
    local set = {}
    for _, l in ipairs(list) do set[l] = true end
    return set
end

local function read_tags_from(name)
    local f = io.open(name, 'r')
    if f == nil then return {} end
    local c = f:read("*a")
    f:close()
    return (yaml.load(c).tags or {})
end

local function extend_with(meta, tags)
    local outfile = convert_to_published(meta.infile)
    if tags == nil then tags = {} end
    if not is_empty(meta.tags) then
        for _, t in pairs(meta.tags) do
            local tag = pandoc.utils.stringify(t)
            if tags[tag] == nil then
                tags[tag] = { outfile }
            else
                local search = Set(tags[tag])
                if search[outfile] == nil then
                    table.insert(tags[tag], outfile)
                end
            end
        end
    end
    return tags
end

local function write_tags_to(name, tags)
    local f = io.open(name, 'w')
    if f == nil then debug("OHSHIT") end
    f:write(yaml.dump({['tags'] = tags}))
    f:write("...\n")
    return nil
end

return {
    {
        Meta = function(meta)
            local tags = read_tags_from(TAG_FILE)
            tags = extend_with(meta, tags)
            write_tags_to(TAG_FILE, tags)
            return nil
        end,
    }
}

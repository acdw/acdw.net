-- Generate a slug for the article id
local text = require('text')

return {
    {
        Meta = function (m)
            local t = pandoc.utils.stringify(m.title)
            t = text.lower(t:gsub("%W+","-"))
            m['slug'] = pandoc.MetaString(t)
            return m
        end,
    }
}

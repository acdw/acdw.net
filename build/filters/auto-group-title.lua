-- Automatically derive the group title from 'infile'

return {
    {
        Meta = function (m)
            if m.infile then
                local ext = string.gsub(m.infile, "(.*)%.(.*)", "%2")
                m["group-title"] = ext
            else
                m["group-title"] = m.title
            end
            return m
        end,
    }
}

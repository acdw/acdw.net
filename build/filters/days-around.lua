-- Figure out links for the days around the current post's date.
-- Input: Meta.infile (src/xxxxx.ext)
-- Output: Meta.yesterday, Meta.tomorrow

local function dayfromfile(fname)
    t = string.gsub(fname,"^.*/(.*)%..*$","%1")
    return tonumber(t)
end

return {
    {
        Meta = function (m)
            local today = dayfromfile(m.infile)
            m.yesterday = string.format("%05d", today - 1)
            m.tomorrow = string.format("%05d", today + 1)
            return m
        end,
    }
}

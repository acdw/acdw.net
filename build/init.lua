-- Pandoc's lua initiation file

-- Initiate pandoc library
pandoc = require("pandoc")
pandoc.utils = require("pandoc.utils")
pandoc.mediabag = require("pandoc.mediabag")

-- Functions and variables
function debug(msg)
    io.write("\27[31m::::")
    if type(msg) == "table" then
        io.write("\n")
        require("pl.pretty").dump(msg)
    else
        io.write(" "..msg)
    end
    io.write("::::\27[0m\n")
end

TAG_FILE = "build/include/tags.yaml"

-- Update the path to include filters/
package.path = package.path .. ";build/filters/?.lua"

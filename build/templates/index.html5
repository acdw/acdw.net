<!DOCTYPE html>
<!-- acdw.net -->
<html xmlns="https://www.w3.org/1999/xhtml"
      lang="en" dir="ltr">
<head>
    <title>$if(title)$$title$ | $endif$acdw</title>
    <meta charset="utf-8" />
    <meta name="generator" content="pandoc" />
    <meta name="viewport"
          content="width=device-width,
          initial-scale=1.0,
          user-scalable=yes" />
$if(author-meta)$
    <meta name="author" content="$author-meta$" />
$else$
    <meta name="author" content="Case Duckworth" />
$endif$
$if(date-meta)$
    <meta name="dcterms.date" content="$date-meta$" />
$endif$
$if(keywords)$
    <meta name="keywords"
          content="$for(keywords)$$keywords$$sep$, $endfor$" />
$endif$
$if(highlighting-css)$
    <style type="text/css">
$highlighting-css$
    </style>
$endif$
    <link rel="stylesheet"
        media="screen"
        href="https://fontlibrary.org/face/cmu-typewriter"
        type="text/css" />
    <link rel="stylesheet"
        media="screen"
        href="https://fontlibrary.org/face/cmu-concrete"
        type="text/css" />
    <link rel="stylesheet"
        media="screen"
        href="/style/base.css"
        type="text/css" />
$if(group-title)$
    <link rel="stylesheet"
        media="screen"
        href="/style/$group-title$.css"
        type="text/css" />
    <link rel="alternate"
          type="application/rss+xml"
          href="/feed/$group-title$.rss"
          title="RSS feed for $group-title$" />
$else$
    <link rel="alternate"
          type="application/rss+xml"
          href="/feed/all.rss"
          title="RSS feed for acdw.net" />
$endif$
    <link rel="icon" type="image/png" href="/favicon.png" />
    <link rel="canonical" href="$canonical$" />
    <link rel="me" href="https://twitter.com/caseofducks" />
    <link rel="me" href="https://micro.blog/acdw" />
    <link rel="me" href="https://github.com/duckwork" />
    <link rel="me" href="mailto:me@acdw.net" />
    <link rel="openid.delegate" href="https://www.acdw.net" />
    <link rel="openid.server"
          href="https://openid.indieauth.com/openid" />
    <link rel="authorization_endpoint" href="https://indieauth.com/auth" />
    <link rel="token_endpoint" href="https://tokens.indieauth.com/token" />
    <link rel="micropub" href="https://www.acdw.xyz/minpub.php" />
$for(css)$
    <link rel="stylesheet" href="/style/$css$" />
$endfor$
$if(math)$
    $math$
$endif$
    <!--[if lt IE 9]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv-printshiv.min.js">
    </script>
    <![endif]-->
$for(header-includes)$
    $header-includes$
$endfor$
</head>
<body id="$title$">
    $for(include-before)$
    $include-before$
    $endfor$

    <header class="site">
        <h1 class="site-title"><a href="/">acdw</a></h1>
        $if(group-title)$
        <h2 class="group-title">
            <a href="/$group-title$/">$group-title$</a>
        </h2>
        <div class="spacer"> </div>
        <a class="rss" href="/feed/$group-title$.rss">
            <object data="/images/rss.svg" type="image/svg+xml">
                <img src="/images/rss.png" />
            </object>
        </a>
        $endif$
    </header>

    <main class="site">
    <article>
$body$
    </article>
    </main>

    <footer class="site">
        <span class="bumper">
        Like this site, or just me as a person?  Feel free to
        <a class="underline" href="https://www.buymeacoffee.com/acdw">
            buy me a tea or something</a>!
        </span>
        <span class="copyright">&copy; 2018
        <a class="p-author h-card" href="https://www.acdw.net">
            Case Duckworth
        </a></span>
        <a rel="license" class="license"
        href="http://creativecommons.org/licenses/by-nc-sa/4.0/">
            <img
            alt="Creative Commons License" style="border-width:0"
            src="https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png"
            />
        </a>
    </footer>

    $for(include-after)$
    $include-after$
    $endfor$
</body>
</html>

<!DOCTYPE html>
<!-- acdw.net -->
<html xmlns="https://www.w3.org/1999/xhtml"
      lang="en" dir="ltr">
<head>
    <title>$if(title)$$title$ | $endif$acdw</title>
    <meta charset="utf-8" />
    <meta name="generator" content="pandoc" />
    <meta name="viewport"
          content="width=device-width, 
          initial-scale=1.0, 
          user-scalable=yes" />
$if(author-meta)$
    <meta name="author" content="$author-meta$" />
$else$
    <meta name="author" content="Case Duckworth" />
$endif$
$if(date-meta)$
    <meta name="dcterms.date" content="$date-meta$" />
$endif$
$if(keywords)$
    <meta name="keywords" 
          content="$for(keywords)$$keywords$$sep$, $endfor$" />
$endif$
    <link rel="icon" type="image/png" href="/favicon.png" />
    <link rel="canonical" href="$canonical$" />
$if(highlighting-css)$
    <style type="text/css">
$highlighting-css$
    </style>
$endif$
    <link rel="stylesheet" 
        media="screen" 
        href="https://fontlibrary.org/face/cmu-typewriter"
        type="text/css" />
    <link rel="stylesheet" 
        media="screen" 
        href="https://fontlibrary.org/face/cmu-concrete"
        type="text/css" />
    <link rel="stylesheet" 
        media="screen" 
        href="/style/base.css"
        type="text/css" />
$if(group-title)$
    <link rel="stylesheet" 
        media="screen" 
        href="/style/$group-title$.css"
        type="text/css" />
    <link rel="alternate" 
          type="application/rss+xml"
          href="/feed/$group-title$.rss"
          title="RSS feed for $group-title$" />
$else$
    <link rel="alternate" 
          type="application/rss+xml"
          href="/feed/all.rss"
          title="RSS feed for acdw.net" />
$endif$
$for(css)$
    <link rel="stylesheet" href="/style/$css$" />
$endfor$
$if(math)$
    $math$
$endif$
    <!--[if lt IE 9]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv-printshiv.min.js"> 
    </script>
    <![endif]-->
$for(header-includes)$
    $header-includes$
$endfor$
</head>
<body class="h-entry">
    $for(include-before)$
    $include-before$
    $endfor$

    <header class="site">
        <h1 class="site-title"><a href="/">acdw</a></h1>
        $if(group-title)$
        <h2 class="group-title">
            <a class="p-category" href="/$group-title$">$group-title$</a>
        </h2>
        $endif$
        <h3 class="page-title p-name">
            <a class="u-url" href="$canonical$">$title$</a>
        </h3>
        <div class="spacer"> </div>
        <time class="date dt-published" 
              datetime="$date$">$datef$</time>
    </header>
    $if(tags)$
    <nav id="tags"><ul>
    $for(tags)$<li><a href="/tag/$tags$">#$tags$</a></li>
    $endfor$
    </ul></nav>
    $endif$

    <main class="site">
    <article id="$slug$" class="e-content"> 
$body$
    </article>
    </main>

    <footer class="site">
        <nav class="datenav">
            <a id="yesterday" href="$yesterday$.html">yesterday</a>
            |
            <a id="tomorrow" href="$tomorrow$.html">tomorrow</a>
            $for(othercats)$
            |
            <a class="othercat"
               href="/$othercats$/$lifeday$.html">$othercats$</a>
            $endfor$
        </nav>
        <span class="copyright">&copy; 2018
        <a class="p-author h-card" href="https://www.acdw.net">
            Case Duckworth
        </a></span>
        <a rel="license" class="license"
           href="http://creativecommons.org/licenses/by-nc-sa/4.0/">
            <img
            alt="Creative Commons License" style="border-width:0"
            src="https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png"
            />
        </a>
    </footer>

    $for(include-after)$
    $include-after$
    $endfor$
</body>
</html>

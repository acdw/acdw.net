<!DOCTYPE html>
<!-- acdw.net -->
<html xmlns="https://www.w3.org/1999/xhtml"
      lang="en" dir="ltr">
<head>
    <title>404 | acdw</title>
    <meta charset="utf-8" />
    <meta name="viewport"
          content="width=device-width, 
          initial-scale=1.0, 
          user-scalable=yes" />
    <meta name="author" content="Case Duckworth" />
<link rel="stylesheet" 
      media="screen" 
      href="https://fontlibrary.org/face/cmu-typewriter"
      type="text/css" />
<link rel="stylesheet" 
    media="screen" 
    href="https://fontlibrary.org/face/cmu-concrete"
    type="text/css" />
<link rel="stylesheet" 
      media="screen" 
      href="/style/base.css"
      type="text/css" />
<link rel="icon" type="image/png" href="/favicon.png" />
    <!--[if lt IE 9]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv-printshiv.min.js"> 
    </script>
    <![endif]-->
</head>
<body>
<?php 
    preg_match("/(\d+)/", $_SERVER['REQUEST_URI'], $lifedays);
    $path = preg_split("/(\d+)/", $_SERVER['REQUEST_URI']);
    $lifeday = $lifedays[0];
    $tom = $lifeday + 1;
    $yst = $lifeday - 1;
    $tomorrow = $path[0] . $tom . $path[1];
    $yesterday = $path[0] . $yst . $path[1];
    preg_match("/\/(\w+)\//", $_SERVER['REQUEST_URI'], $groups)
?>
    <header class="site">
        <h1 class="site-title"><a href="/">acdw</a></h1>
        <h2 class="group-title">
<?php echo '<a href="/' . $groups[1] . '">' . $groups[1] . '</a>' ?>
        </h2>
        <h3 class="page-title">404</h3>
        <h4 class="date"><?php
    $birth = date_create('1990-07-25');
    date_add($birth, date_interval_create_from_date_string($lifeday . ' days'));
    echo date_format($birth, 'Y-m-d');
?>
    </h4>
    </header>

    <main class="site">
    <article id="404"> 
        <p>This page doesn't exist, sorry.</p>
    </article>
    </main>

    <footer class="site">
        <nav class="datenav">
            <a id="yesterday" 
                href="<?php echo $yesterday ?>">yesterday</a>
            <a id="tomorrow" 
                href="<?php echo $tomorrow ?>">tomorrow</a>
        </nav>
        <span class="copyright">&copy; 2018 Case Duckworth</span>
        <a rel="license" class="license"
        href="http://creativecommons.org/licenses/by-nc-sa/4.0/">
            <img
            alt="Creative Commons License" style="border-width:0"
            src="https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png"
            />
        </a>
    </footer>

    </body>
</html>

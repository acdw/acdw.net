# MAKEFILE v3
# "Life is really simple, but we insist on making it complicated."
# <acdw.net>
# TODO: tags!

# Source: where all the plain textfiles go
SRC = src
# Publish: where the finished product goes
PUB ?= pub
# Assets: where images, styles, and cetera go
ASSETS = assets
# Build: where build-related files go (this is pandoc's data-dir)
BUILD = build
# Feeds
FEED = $(PUB)/feed
# Includes
INCLUDE = $(BUILD)/include
# Tag source
TAGS = $(INCLUDE)/tags.yaml
TAGGR = $(BUILD)/taggr.py
TAGPUB = $(PUB)/tag
# My home-spun indexer and options
INDEXR_BIN = $(BUILD)/indexr.py
INDEXR_OPT = -r '| acdw' -x index.html -R
INDEXR = $(INDEXR_BIN) $(INDEXR_OPT)

CAT := /usr/bin/cat
# Now with GPP - the generic preprocessor
# Just another step in making this totally un-portable
GPP_BIN := /usr/bin/gpp
GPP_OPT = -U "<%" ">" "\\B" "|" ">" "<" ">" "%" ""
GPP_OPT += -I$(INCLUDE)
GPP = $(GPP_BIN) $(GPP_OPT)

# Pandoc binary and options
# I think I've made this fairly easy to expand to other formats,
# even though right now it's just HTML.
PANDOC_BIN = /usr/bin/pandoc 
PANDOC_OPT = --standalone --data-dir=$(BUILD) --email-obfuscation=references
PANDOC_OPT_HTML = --to=html5 --html-q-tags --ascii --section-divs
PANDOC = $(PANDOC_BIN) $(PANDOC_OPT) $(PANDOC_OPT_HTML)

# Groups of posts go in their own folders (like WordPress categories?)
IGNORE := root feed
GROUPS = $(filter-out $(IGNORE), \
            $(sort $(foreach p,$(wildcard $(SRC)/*),\
              $(subst .,,$(suffix $(p))))))
PUBGROUPS = $(foreach g,$(GROUPS),$(PUB)/$(g))
# Roots are unchanging, like pages.  Also included: index.html
ROOTIGNORE := index
ROOTIGNORE := $(foreach ri,$(ROOTIGNORE),$(SRC)/$(ri).root)
ROOTS = $(patsubst $(SRC)/%.root,$(PUB)/%/index.html,\
           $(filter-out $(ROOTIGNORE),$(wildcard $(SRC)/*.root)))
# ROOTS += $(PUB)/index.html
# Feeds are rss feeds (duh).  I have one for each group and a combo-punch.
FEEDS = $(foreach g,$(GROUPS),$(FEED)/$(g).rss)
FEEDS += $(FEED)/acdw.rss

# misc. variables
CANONICAL := https://www.acdw.net

# Build everything and serve it.
.PHONY: serve all
all: $(PUB)/index.html $(FEEDS) $(FEED)/index.html $(TAGS) | $(PUB)
	rsync -a $(ASSETS)/ $(PUB)/
	$(TAGGR) $(TAGPUB)
serve : all
	php -S localhost:8000 -t $(PUB)/

# Build the site-level index and feed.
$(PUB)/index.html: PUBLISHED = $(foreach g,$(PUBGROUPS),$(wildcard $(g)/*))
$(PUB)/index.html: $(SRC)/index.root \
  $(INDEXR_BIN) $(addsuffix /index.html,$(PUBGROUPS)) \
  $(ROOTS) \
  $(BUILD)/filters/_index.lua
	($(INDEXR) -f yaml $(PUBLISHED); \
	 $(INDEXR_BIN) -r '| acdw' -f yaml -x 401 -x 403 -x 500 -x 404 \
	               -n 'roots' $(ROOTS)) \
	| $(CAT) - $< | $(GPP) \
	| $(PANDOC) -o $@ --template=index.html5 \
	-V canonical="$(CANONICAL)/" -M title=acdw \
	--lua-filter=_index.lua

$(FEED)/acdw.rss: PUBLISHED = $(foreach g,$(PUBGROUPS),$(wildcard $(g)/*))
$(FEED)/acdw.rss: $(PUBLISHED) $(INDEXR_BIN) | $(FEED)
	$(INDEXR) -f rss2 $(PUBLISHED) > $@

$(FEED)/index.html: $(SRC)/index.feed $(FEEDS) $(INDEXR_BIN) \
  $(BUILD)/filters/_index.lua | $(FEED)
	$(INDEXR_BIN) -f yaml $(FEEDS) -x acdw.rss -r 'acdw ' \
	| $(CAT) - $< | $(GPP) \
	| $(PANDOC) -o $@ --template=index.html5 \
	-V canonical="$(CANONICAL)/feed/" -M title=feed \
	--lua-filter=_index.lua

# Group-level: this takes some magic.
# Basically what we're doing is setting up a macro to generate recipes for
# groups: we need the posts themselves (in pub/<group>/<date>.html) and the
# index for each group (pub/<group>/index.html).  To make those work we need a
# filter for pandoc and an index.<group> in src/.  I don't really like messing
# around with src/ in this Makefile, but I think this is the only time I
# really need to -- and the automatic group generation is worth it.
define GROUP_template
$(eval $(1)s = \
	$(filter-out %index.html, \
	$(patsubst $(SRC)/%.$(1),$(PUB)/$(1)/%.html,\
	$(wildcard $(SRC)/*.$(1)))))

$(PUB)/$(1)/index.html: $(SRC)/index.$(1) \
  $(value $(1)s) $(INDEXR_BIN) $(BUILD)/templates/index.html5 \
  $(BUILD)/filters/_index.lua
	$(INDEXR) -f yaml $(value $(1)s) \
	| cat - $$< | $(GPP) \
	| $(PANDOC) -o $$@ --template=index.html5 \
	-M title=$(1) -V canonical="$(CANONICAL)/$(1)/" \
	--lua-filter=_index.lua

$(FEED)/$(1).rss: $(value $(1)s) $(INDEXR_BIN) | $(FEED)
	$(INDEXR) -f rss2 --ftitle 'acdw $(1)' --fhref '$(CANONICAL)/$(1)/' \
		$(value $(1)s) > $$@

$(PUB)/$(1)/%.html: $(SRC)/%.$(1) \
  $(BUILD)/templates/default.html5 $(BUILD)/filters/_$(1).lua | $(PUB)/$(1)
	$(GPP) $$< \
	| $(PANDOC) -o $$@  \
	--lua-filter=_$(1).lua \
	-V canonical="$(CANONICAL)/$(1)/$$(notdir $$@)" -M infile=$$<

$(BUILD)/filters/_$(1).lua: $(BUILD)/filters/_post.lua
	[ -e $$@ ] || echo 'return { table.unpack(require("_post")) }' > $$@
	touch $$@
$(SRC)/index.$(1): 
	touch $$@
endef
$(foreach g,$(GROUPS), $(eval $(call GROUP_template,$(g))))

# Make the necessary directories, if necessary
$(PUBGROUPS) $(FEED):
	mkdir -p $@

# Make roots
$(PUB)/%/index.html: $(SRC)/%.root $(BUILD)/filters/_root.lua
	mkdir -p $(dir $@)
	$(GPP) $< \
	| $(PANDOC) -o $@ --lua-filter=_root.lua

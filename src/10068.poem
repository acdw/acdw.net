% Your name
% duck; ode
% 2018-02-16T00:00:00-0600


| When you're gone I chew on your name
| like cud.  I ruminate.  I dream in the daylight
| under perfect white
| clouds fleecing across the sky like ducks
| on a pond, or stones skipping
| a river to the other side, where you
| stand tall and proud.  I need no metaphor
| for your shoulders, nor for your hair
| blowing in the wind that cannot rip your name away.
| You are complete in yourself, dear heart,
| and the fish in the stream know it.  The cows
| downriver bathing know it, and the stones
| and the sky and the mountains tall as fences.
| You are the center pin holding the world together,
| the sun in Copernicus's eyes
| as he tore them from the telescope, dazzled.

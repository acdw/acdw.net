% The Insides, by Jeremy P. Bushnell
% magic; not-great; fantasy 
% 2018-05-25T13:11:56-0500

This book was good, but it took me a while to read.  Something about it made
it hard for me to really get into in the way it feels people must mean when
they say a book is "fresh" or that it "had me on the edge of my seat" or one
of those other near-cliches that circulate around reviews nowadays (I don't
really read many reviews (maybe I should)).  I didn't get lost in it the way I
got lost in the [last][ancillary] [two books][butler] I read, which maybe says
more about the last two books I read than about this book, or more about my
tastes.  At any rate, I was satisfied on finishing *The Insides*, but not for
all of the right reasons.

First, let's talk about the good: Bushnell has done well with the language
here, the dialogue (easy in a kind of cinematic way, breezy, even) and the
narration (heavy on slang, but in a conversational instead of *Clockwork
Orange* way) do a lot of world-building work, which is fine since the world is
basically ours but with a patina of magic.  He also switches between the two
main characters, Ollie and Maja, deftly, juggling their very different
personalities and concerns with skill.  I also liked what I could get out of
his conception of magic as just finding, or making, importance in the world:
it's kind of a more explicit reading of a trend in magical writing that seems
to be going on more recently, kind of a "low fantasy" thing that's in vogue.

The problem is, magic doesn't get much more of a treatment than that --
there's no real magical battles or drama outside of this thing called the
Inside, which is described as the area behind the stage of the world, and one
creature that comes out of it.  Maja is able to magically track anything or
anyone at all, and to divine their histories by looking at them, but past the
first few chapters that ability is taken for granted, and its god-like power
becomes pedestrian, almost.  There's a bit about ancient magical weapons that
feels shoe-horned in at the end, even though it's the whole reason the book
goes forward; the real problem here is that the villain, or really anyone,
doesn't have a clearly-defined motivation behind their actions.  Magic is best
used to force a character's will on the world, but none of the characters in
this novel really use it for more than finding things, getting promotions, or
making people fall in love with them.  The patina of magic is simply too thin.

[ancillary]: https://www.acdw.net/read/10150.html
[butler]: https://www.acdw.net/read/10158.html

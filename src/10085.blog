% Installing tinytinyrss and wallabag on nearlyfreespeech.net
% metablog; self-hosting; typo
% 2018-03-05T00:00:00-0600

I've recently installed [tinytinyrss] and [wallabag] on this server (I'm not
sharing the links with *you*; they're for me!) and it mostly went pretty well
-- just some permissions-changing, really. Of course, it took me the better
part of four hours to install wallabag, but it turned out after all of that it
was because I made a typo in the name of my database: it's called
`personal.db` but I had it in parameters.yml as `personal.sql`. I only cried a
little bit!

Now that I've recovered from the trauma, here's some instructions to get them
running on your own [NFSN] instance. I'm writing these down because there was
scant information online already about setting them up, and for a newbie like
me, it would've been very useful.[^1]

# Pre-installation

Before installing either of these applications on your little slice of
Internet, you'll need to enable MySQL if you haven't already. To do that, sign
in to NFSN and click the `mysql` tab. On the sidebar, click
`Create a New MySQL Process` and choose what you want in the options (I picked
the default, `MariaDB 10.2 + InnoDB`), and click `Create New Process`. The
first MySQL process on NFSN is free (outside the resource and storage costs,
of course). I was able to put both of these services in one database, so one
should be plenty unless maybe you're planning on having a public instance of
either of these.

Once you create the database, you'll get an email from NFSN about your new
instance. Follow the instructions to change your admin password and create a
new user for you. You can find the phpMyAdmin at
<https://phpmyadmin.nearlyfreespeech.net>. Once all that's done, you're ready
to install other stuff![^2]

# Installing tinytinyrss

If you don't know,

> Tiny Tiny RSS is an open source web-based news feed (RSS/Atom) reader and
> aggregator, designed to allow you to read news from any location, while
> feeling as close to a real desktop application as possible. -- tt-rss.org

It's basically Google Reader or Feedly or whatever, but self-hosted. I compare
it to Google Reader because most sites I read to figure out how to set it up
did, and I think they did because they wrote them around the time that Google
stopped Reader as a service, and I think the reason why there's no newer
information is because people, as a whole, don't really use RSS anymore. They
use Facebook feeds and Reddit and all that business (I was thinking about
using tt-rss to collect my Facebook notifications off-site to read at my
leisure, but alas, they disabled that feature in 2013). However, I like RSS
feeds (by the way, you can subscribe to [my feed here], if you want) because
it collects *my* sources that *I'm* interested in into one spot for easy
reading. So here's the instructions (great thanks, if they need it, to [Nomad
Physicist] and [robinadr] -- I read them to figure this out, and of course
you'll want to reference [the official documentation]):

## Download

First, you need to get the tt-rss source, so you'll need `git`. NFSN already
has `git` installed, but the entire repo's history is quite large and I didn't
want to pay for NFSN to host it all, so I cloned it on my home computer and
used `scp` to copy everything besides `.git` to the server. Here's the basic
steps:

``` {.bash}
git clone https://tt-rss.org/git/tt-rss.git tt-rss
cd tt-rss 
for file in *; do scp "${file}" <nsfnuser>@<nsfnhost>:/home/public/tt-rss; done 
```

## Install

Once you have everything copied over, you'll need to set up your database to
handle tinytinyrss. Head over to <https://phpmyadmin.nearlyfreespeech.net> and
sign in to your database. Click the `User accounts` tab and click
`Add user account`. I used `tinytinyrss` as the username and generated a
password. (Keep the `Host name` field the same.) Check the
`Create database with same name and grant all priveleges`, scroll down to the
bottom, and click `Go`. You can close the page after that.

Now, you should be able to navigate to `<your-site>/tt-rss/install` and follow
the instructions there. The rest of the setup is detailed on [tt-rss.org][the
official documentation], it's really pretty easy. Really, setting up Tiny Tiny
RSS was not the reason I wrote this post; now that I'm done with this section
I'm not sure if I should've written it at all -- it's that easy.

## Afterward

After you're finished installing, the last thing you'll need to do is set up a
cron job with NFSN to run the update script every hour (I also created a new
user to aggregate feeds under, but I'll leave that as an exercise). Head over
to your nearlyfreespeech member panel and select your site in the `sites` tab.
On the sidebar you'll find a link to `Manage Scheduled Tasks`; click it and
Add `/usr/local/bin/php /home/public/tt-rss/update.php --feeds --quiet` (as
user `web`, in the ssh environment) every hour, or as often as you want ---
though I don't think you'll get much use out of updating less frequently than
a day. If you want to update your feeds right away (which you'll need to do
whenever you add a feed if you don't want to wait for the hour), ssh into your
site and run the same command.

# Installing wallabag

Wallabag is a Read-it-Later bookmark service, like [Pocket] or [Instapaper],
but again, it's self hosted (technically they do have a hosted instance at
<https://wallabag.it>, but it costs money). I have been using Pocket, but
since I have this site now and wanted a project, I set up a wallabag instance.
It was a little harder to set up because the documentation is not nearly as
good as tt-rss, but here's what I did. The manual can be read [online], for
what it's worth, but like I said, it's not very comprehensive.

## Download

You want to use the directions from the \[On shared hosting\] section of the
install guide, since that's what NFSN is. Basically, you'll

    wget https://wllbg.org/latest-v2-package && tar xvf latest-v2-package

in an `ssh` to your server, then rename the resulting folder to whatever you
want the installation to be. I used `walla` because `wallabag` is too long to
type. You can delete the `latest-v2-package` tarball.

## Install

You'll need to create another user and database for your MySQL database for
wallabag; it's basically the same instructions as for tt-rss, except you
substitute 'wallabag' for 'tinytinyrss'.

In your wallabag installation folder, edit `app/config/parameters.yml` with
your database configuration. It should look something like this[^3]:

``` {.yaml}
parameters:
    database_driver: pdo_mysql
    database_driver_class: null
    database_host: <database>
    database_port: null
    database_name: wallabag
    database_user: wallabag
    database_password: <password>
    database_path: null
    database_table_prefix: wallabag_
    database_socket: null
    database_charset: utf8mb4
    domain_name: '<domain>/web'
```

If you don't want to run a public instance of wallabag, you'll need to include
`fosuser_registration: false` in `parameters.yml` as well. It's on by default.

Once your configuration looks alright, run the following commands and navigate
to your site.

    bin/console --env=prod cache:clear
    chgrp -R web var

You need to `chgrp` because you'll get a permissions error on the server if
the files in `var/` aren't in the `web` group.

It should work! If not, keep tweaking and twiddling for four hours or so until
you realize that a typo is keeping it from working (if you're like me).

## Afterward

I created a new user for myself and began importing articles from Pocket right
away. It worked okay until it didn't, but I didn't want to do all the
troubleshooting when most of my Pocket articles are old anyway and not very
*read-it-later-able*, as well (I threw a lot of videos and [Hacker News]
comment threads in there for a while). I also installed the [Android app] and
[Firefox] extension without much fuss at all -- the instructions for
installing those are more helpful.

One more thing you might want to do on Firefox is disable the built-in Pocket
extension. Head over to `about:config` and change `extensions.pocket.enabled`
to `false`.

# Conclusion

It really wasn't as hard as I thought it'd be to run my own instances of these
applications, and I don't think it'll be very expensive either! It's nice to
know that if I do change my mind, it's pretty simple to delete the whole thing
or shut it down myself. My data is mine, for once.

[^1]: I'm sure you're thinking, but you *learned* that way, in a way that
    reading a blog post like this wouldn't teach you! To you I say, fa! I am
    writing this also for my future self.

[^2]: This article assumes you already have a site using nearlyfreespeech.net
    and that you have it set up with `ssh` or some way to transfer files
    there, as well as whatever DNS you need to make it work.

[^3]: I used the 'web' folder of my domain because I couldn't figure out the
    instructions to [set up a VirtualHost] and have the DocumentRoot at
    `web/`. If you can figure that out, you probably don't need this guide,
    but you also don't need to inlude `web` in the `domain_name` field above.

  [tinytinyrss]: https://tt-rss.org/
  [wallabag]: https://wallabag.org/en
  [NFSN]: https://www.nearlyfreespeech.net/
  [my feed here]: https://www.acdw.net/feed.xml
  [Nomad Physicist]: http://nomadphysicist.busbycreations.com/2013/tiny-tiny-rss-on-nearlyfreespeech-net/
  [robinadr]: https://robinadr.com/2013/05/tiny-tiny-rss-nearly-free-speech
  [the official documentation]: https://git.tt-rss.org/fox/tt-rss/wiki/InstallationNotes
  [Pocket]: https://getpocket.com/
  [Instapaper]: https://www.instapaper.com/
  [online]: https://doc.wallabag.org/en/admin/installation/installation.html
  [Hacker News]: https://news.ycombinator.com
  [Android app]: https://doc.wallabag.org/en/apps/android.html
  [Firefox]: https://addons.mozilla.org/en-US/firefox/addon/wallabagger/?src=search
  [set up a VirtualHost]: https://doc.wallabag.org/en/admin/installation/virtualhosts.html

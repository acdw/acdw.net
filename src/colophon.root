% Colophon


# Technology 

This site is written with [neovim] on [Antergos XFCE] in [Markdown]. It's
converted to html using [pandoc] and hosted by [Nearly Free Speech].  I
publish with a combination of [GNU Make] and [Git].  If you want to peruse the
source, there's a [mirror] of the site on [Gitlab].

[neovim]: https://neovim.io/
[Antergos XFCE]: https://antergos.com/
[markdown]: https://antergos.com/
[pandoc]: http://pandoc.org/
[Nearly Free Speech]: https://www.nearlyfreespeech.net/
[GNU Make]: https://www.gnu.org/software/make/
[Git]: https://git-scm.com/
[mirror]: https://gitlab.com/acdw/acdw.net
[Gitlab]: https://gitlab.com

# Workflow

## Writing

I've written a short script in my `~/bin` folder called, fittingly enough,
`acdw`.[^acdw-script]  When I want to write a new post, say, a poem, I type

    acdw poem

in my shell and it opens an editor with the date pre-populated.  I write out
my poem in Markdown, give it a title, save and quit -- and the script
automatically adds [lineblock marks] to poem posts (for line breaking
purposes).  If I want to write a post for a different day, I just append a
date string (`acdw` uses `date`, so whatever `date` can parse works):

    acdw blog 'last week'

(If the file is empty, it's automatically deleted by the script.)

## Building

My build process is fairly straightforward: I have a number of source files in
`src/` (fittingly) that get processed by pandoc into the requisite files in
`pub/`.  I use a couple of filters that do things like [convert blockquotes to
epigraphs][epigraph] or [automatically links posts written on the same day to
each other][autolink] that tailor the generated site to my needs. I build
everything using a [Makefile] that generates recipes based on what groups
I have posts for and spits everything out into `pub/`, ready for publishing.

[epigraph]: https://gitlab.com/acdw/acdw.net/blob/master/src/filters/blockquote-to-epigraph.lua
[autolink]: https://gitlab.com/acdw/acdw.net/blob/master/src/filters/autolink.lua
[Makefile]: https://gitlab.com/acdw/acdw.net/blob/master/Makefile

## Publishing

Since this site is a git repository, publishing is just a `git push`
away.[^gitpub] Before I pull the trigger, though, I make sure the site looks
alright by running `make serve`, which runs a PHP server rooted in `pub/` so
I can poke around.  (It's a little less useful now that I've moved to almost
exclusively absolute linking.)  Once I've verified it'll be okay, I `git push`
it to my public site <https://www.acdw.net> and my [Gitlab mirror].

[Gitlab mirror]: https://gitlab.com/acdw/acdw.net

<!-- footnotes -->
[^acdw-script]: 
    I've got a gist of it here:
    <script src="https://gist.github.com/duckwork/baa6a3f0dc90cdbd9e295630e606f2cf.js"></script>

[^gitpub]:  I originally got this tip from a now-defunct website, *Nerdess*
([archive.org link](https://web.archive.org/web/20160316095149/http://www.nerdess.net/blog/nerdy/git-by-example-how-to-update-your-website-on-nearlyfreespeech-net-via-git/)).
It's been so useful I've enumerated the basics in a [blog
post](https://www.acdw.net/blog/10000.html)

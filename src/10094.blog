% Adventures in gardening
% garden; Stella; problem-solving; table
% 2018-03-14T00:00:00-0600

We started a garden this year, about two weeks ago. We went to the local
nursery and bought some young (seedling?) tomatoes, jalapeños, bell peppers,
basil, thyme, and parsley. They've been doing pretty well so far, which feels
really good -- I always say, "They haven't died yet!" coming home. Of course,
something that helps us a lot is living in [Zone 8b], meaning the growing
season is long and warm. While the Nor'Easters are raging in New England and
down the Mid-Atlantic, we've had beautiful early-spring weather.

That is, until last night. It's been chilly for the past couple of days,
chilly enough that I haven't been able to get the chill out of my bones in a
satisfying way. And then last night, it was really *quite* cold, and I
realized as I was getting into bed -- *the plants!* I know that tomatoes like
it warm and I was worried it would get too cold for them. I checked the
weather and it read a low of 41F over night, so I pulled myself out of bed and
put all of them inside. I took a little picture of them this morning:

![The plants inside this morning]

and I went to work. They were doing pretty good when I got home for lunch,
too:

![The plants at lunch]

And I went merrily back to work, mind at ease. That is, until I got home to
Stella in her crate and an irate R, eye-gesturing between the dog and the
pots. Finally I caught on: *Stella had dug in the plants!* She'd nearly
snapped one tomato in half, and dirt was disturbed in about half the others. R
did a great job re-soiling the plants, and I think that they'll be okay, but
this was unacceptable from Stella. I was hurt, and mad, and surprised at
myself for being so hurt and mad. Turns out, I was attached to these plants.

![The broken tomato]

I was also attached to this dog, though, so we had to come up with a solution.
It's still going to be cold for the next couple of nights (it's actually
getting down to 31F tomorrow; I am *not pleased*), so we couldn't put them
outside. We also couldn't leave them on the floor, and putting them in another
room was out -- not enough sunlight and we couldn't keep our eagle-eye on the
pup (there was no trust anymore). Finally, I remembered we had a card table in
the mud room. I brought it out, and now our plants are living in a high-rise:

![Table plants]

We did have to put two tomatoes out on the porch during the day, since the
table isn't really built for more than the weight of a pack of cards and maybe
some plates, but we can bring those in when Stella is safely crated for the
night. And they're actually getting a fair amount of light, so I'm pleased. I
think R is too.

  [Zone 8b]: https://planthardiness.ars.usda.gov/PHZMWeb/
    "Here's the map for the whole U.S. It's pretty interesting information."
  [The plants inside this morning]: /images/plants-inside-1.jpg
    "The little ladybug's name is Herman, or Hugo, or Melville, or Tom, or something (R says he's too pure for a name), but at any rate he watches over our crops."
  [The plants at lunch]: /images/plants-inside-2.jpg
    "I had leftover risotto with falafel, if you were wondering."
  [The broken tomato]: /images/plants-inside-3.jpg
    "Breaking my heart, this one."
  [Table plants]: /images/plants-inside-4.jpg
    "I feel like I should make a joke about Trump and high-rises somewhere, but honestly, I'm too tired to think of one worthwhile."

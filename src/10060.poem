% The world of duck
% duck; ode
% 2018-02-08T00:00:00-0600

> How fleeting are all human passions compared with the massive continuity of
> ducks.\
> --- [Dorothy L. Sayers]

| My brothers, taking flight from the reeds.
| The hoarse cries lifting like wings to the clouded sky
| to walk the fine path to heaven. Landing, softly,
| on water -- the blood of the world -- to go about
| the business of your lifetime: fishing, diving,
| hiding again in the reeds, raising your children
| to continue the cycle. The world of duck is round
| as the duck's eye, lidded with reeds, always open.

  [Dorothy L. Sayers]: https://i.redd.it/etmzh1s403f01.jpg

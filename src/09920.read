% Tom Gauld is a friend of mine
% comics; historical-fiction; alt-narrative
% 2017-09-22T00:00:00-0600

*Note: I actually don't know Tom Gauld. I've read his comics, though! And that
is what this post is about.*

# You're All Just Jealous of My Jetpack

I first became familiar with Gauld's work through his [tumblr], which is
titled the same as his book and features the same: cartoons he's drawn for
*The Guardian*. I was originally drawn to his comics by the clean line and
coloring style, and by the literary humor that lampoons genre, the publishing
industry, and popular (or even "canonical") works. That being said, *Jetpack*
is probably best-suited for sitting on a coffee table for occasional leafing
by guests. I read it straight through, and after about forty pages of similar
jokes, found myself rushing to have it over with.

# Goliath

Gauld's first graphic novel is an alternative-perspective on Goliath (as in,
*David and*). I'm a sucker for alternative-perspective stories, from *Grendel*
to *Wicked* (and I have *The Last Ringbearer* ready to read whenever I can get
a Kindle or something), so I was excited to pick up *Goliath*.

The most interesting twist of *Goliath* is its characterization of the title
character: Goliath is just a big guy who is more interested in book-keeping
than fighting, and has been happy in his desk job during the Hebrew-Philistine
war until an enterprising middle-manager of a general convinces the king that
a "Fight of Champions" will win the war with no cost to the Philistines. Of
course, Goliath is that champion because of his size, though he is kept in the
dark about his mission for as long as possible.

Gauld's sparse style lends itself well to this story, most of which has
Goliath sitting at a pile of rocks at the bottom of a gorge and reading the
pre-written challenge to the Israelites. David doesn't even feature except as
a premonition of death from the mist, just before he hits an unprepared
Goliath in the head and kills him, ending the story. The boy the story is more
interested in is Goliath's shield-bearer, who looks up to Goliath and is
probably the only person to mourn his death.

I was surprised by *Goliath*, both by its shortness and depth: Gauld has taken
scant source material on one of the Bible's most infamous characters and given
him, if not a full life, a sketch that points to his humanity, and reminds us
that there is never only one side to a conflict.

  [tumblr]: http://myjetpack.tumblr.com/

<!DOCTYPE html>
<!-- acdw.net -->
<html xmlns="https://www.w3.org/1999/xhtml"
      lang="en" dir="ltr">
<head>
    <title>Words and Meaning | acdw</title>
    <meta charset="utf-8" />
    <meta name="generator" content="pandoc" />
    <meta name="viewport"
          content="width=device-width, 
          initial-scale=1.0, 
          user-scalable=yes" />
    <meta name="author" content="Case Duckworth" />
    <link rel="icon" type="image/png" href="/favicon.png" />
    <link rel="canonical" href="https://www.acdw.net/read/09844.html" />
    <link rel="stylesheet" 
        media="screen" 
        href="https://fontlibrary.org/face/cmu-typewriter"
        type="text/css" />
    <link rel="stylesheet" 
        media="screen" 
        href="https://fontlibrary.org/face/cmu-concrete"
        type="text/css" />
    <link rel="stylesheet" 
        media="screen" 
        href="/style/base.css"
        type="text/css" />
    <link rel="stylesheet" 
        media="screen" 
        href="/style/read.css"
        type="text/css" />
    <link rel="alternate" 
          type="application/rss+xml"
          href="/feed/read.rss"
          title="RSS feed for read" />
    <!--[if lt IE 9]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv-printshiv.min.js"> 
    </script>
    <![endif]-->
</head>
<body class="h-entry">
    
    <header class="site">
        <h1 class="site-title"><a href="/">acdw</a></h1>
                <h2 class="group-title">
            <a class="p-category" href="/read">read</a>
        </h2>
                <h3 class="page-title p-name">
            <a class="u-url" href="https://www.acdw.net/read/09844.html">Words and Meaning</a>
        </h3>
        <div class="spacer"> </div>
        <time class="date dt-published" 
              datetime="2017-07-08T00:00:00-0600">2017-07-08</time>
    </header>
        <nav id="tags"><ul>
    <li><a href="/tag/wizards">#wizards</a></li>
    <li><a href="/tag/magic">#magic</a></li>
    <li><a href="/tag/le-guin">#le-guin</a></li>
    <li><a href="/tag/language">#language</a></li>
    <li><a href="/tag/fantasy">#fantasy</a></li>
    
    </ul></nav>
    
    <main class="site">
    <article id="words-and-meaning" class="e-content"> 
<p>Stories about wizards have always held great charm for me, and in reading <em>A Wizard from Earthsea</em>, I have finally come to understand why. Wizards&#8217; magic fulfills the promise of language, that words have meaning and are powerful, that they have their own existences beyond their uses as hooks to meaning, as metaphor. I remember in <em>Gulliver&#8217;s Travels,</em> Swift describes a land where people have foregone words, choosing instead to carry all the objects of their conversations on their backs; for Le Guin, words <em>are</em> the things that are carried, to use for good or ill, as weapons for as balms, for friend or foe.</p>
<p>As a writer (or at the very least, a Word Nerd), I&#8217;m fascinated with my use of words, and by extension our uses. It&#8217;s trivial to say that words are important, that they can affect change in the world through persuasion, flattery, or plain tricks, but I think &#8211; I&#8217;ve thought for a long time &#8211; that words are more real than that, that they have some sort of reality beyond what they signify. For example, I heard on the radio (some <em>TED Radio Hour</em>, I think, though I might&#8217;ve read it or seen it or just thought it, you know how that is<a href="#fn1" class="footnote-ref" id="fnref1"><sup>1</sup></a>) that without language, humans can&#8217;t really tell right from left, or make more complex spacial relationships regarding our world. The experiment started with mice: they got put in a square room with different patterns on each wall, and shown where to get some food &#8211; say, it&#8217;d be left of the checked wall. If they were picked up, turned around, and made to try to find the food again (in the same spot), they had to start over. They couldn&#8217;t work out, <q>I need to get to the left of the checked wall.</q> Okay, you say; that&#8217;s just a mouse; who cares? <em>Well:</em> the same thing happened with people when researchers knocked their language facilities out of their heads (temporarily, of course). So in that small way, at least, words wield real power in our lives: they root us where we are and allow us to get around the world.</p>
<p>In <em>Earthsea</em>, of course, words do more than that. There&#8217;s this concept of the True Language of the world, and every thing<a href="#fn2" class="footnote-ref" id="fnref2"><sup>2</sup></a> has its own name in the True Speech, by which it can be called and manipulated by a wizard. Additionally, if you want to change the nature of a thing, you have to change its name, which will ripple out to the other things around it, changing in some way their natures. Basically, as far as I can figure, Earthsea is defined as a number of objects in loose relation to one another, where they each have their own nature but are defined in terms of each other as well. I suppose all that&#8217;s an implementation detail; the real interesting thing is that words and names are tightly bound to the things they represent, so much so that they cease to represent them and actually <em>are</em> those things. There&#8217;s a passage in the book that mentions that the entire Universe is just the syllables of one long word, spoken slowly and inexorably by the stars. There&#8217;s a beautiful sort of completeness in that, an echoing of the gospel of John or of (I think) Jewish mysticism: once the word&#8217;s done, it&#8217;s done. The breath is out. Something something heat death of the universe, right?</p>
<p>What are we to make of this? I&#8217;m not sure. Obviously words don&#8217;t have the kind of power they do in Earthsea, but they definitely have more power than <em>Sticks and stones may break my bones</em> nursery-rhymes would have us believe, and I think they have more power even then the silver-tongued confidence man would have you believe. They&#8217;re somewhere in the realm of unicorns, maybe: because a unicorn has been described and defined, even though you&#8217;ll never see one or hear its breath, it must exist somewhere, right, somehow? Or like the one idea of death I heard about another time on TED: that after we die, we&#8217;re all ushered to a waiting room to hang out until our second death, when our name is said by the living for the last time. There&#8217;s a real tragedy in that last death, in the knowledge that one day, all knowledge will be lost, even if it will just as surely be refound: because at that refinding, is it still the same knowledge? And how many times has it happened already?</p>
<section class="footnotes">
<hr />
<ol>
<li id="fn1"><p>Incidentally, that&#8217;s a great idea for a longer post. Maybe I&#8217;ll come back to that at some point, who knows.<a href="#fnref1" class="footnote-back">&#8617;</a></p></li>
<li id="fn2"><p>Which, incidentally, I just learned comes from Old Norse meaning <em>a meeting of disparate elements</em>. Fun fact!<a href="#fnref2" class="footnote-back">&#8617;</a></p></li>
</ol>
</section>
    </article>
    </main>

    <footer class="site">
        <nav class="datenav">
            <a id="yesterday" href="09843.html">yesterday</a>
            |
            <a id="tomorrow" href="09845.html">tomorrow</a>
                    </nav>
        <span class="copyright">&copy; 2018
        <a class="p-author h-card" href="https://www.acdw.net">
            Case Duckworth
        </a></span>
        <a rel="license" class="license"
           href="http://creativecommons.org/licenses/by-nc-sa/4.0/">
            <img
            alt="Creative Commons License" style="border-width:0"
            src="https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png"
            />
        </a>
    </footer>

    </body>
</html>
